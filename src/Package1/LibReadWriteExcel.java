package Package1;

/*import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ReadExcelTestAndQuiz {
	public void readExcel(String filePath,String fileName,String sheetName) throws IOException{
   //Create a object of File class to open xlsx file
   File file =    new File(filePath+"\\"+fileName);
   //C:\workspace\POI\src\excelExportAndFileIO\ExportExcel.xlsx
    //Create an object of FileInputStream class to read excel file
    FileInputStream inputStream = new FileInputStream(file);
    
    Workbook excelWorkbook = null;
    //Find the file extension by splitting file name in substring and getting only extension name
    String fileExtensionName = fileName.substring(fileName.indexOf("."));
    //Check condition if the file is xlsx file
    if(fileExtensionName.equals(".xlsx")){
    //If it is xlsx file then create object of XSSFWorkbook class
    	excelWorkbook = new XSSFWorkbook(inputStream);
    }
    //Check condition if the file qis xls file
    else if(fileExtensionName.equals(".xls")){
        //If it is xls file then create object of XSSFWorkbook class
    	excelWorkbook = new HSSFWorkbook(inputStream);
    }
    //Read sheet inside the workbook by its name
    Sheet excelSheet = excelWorkbook.getSheet(sheetName);
    //Find number of rows in excel file
    int rowCount = excelSheet.getLastRowNum()-excelSheet.getFirstRowNum();

    //Create a loop over all the rows of excel file to read it
    for (int i = excelSheet.getFirstRowNum(); i < rowCount+1; i++) {
        Row row = excelSheet.getRow(i);
        //Create a loop to print cell values in a row
        for (int j =row.getFirstCellNum(); j < row.getLastCellNum(); j++) {
            //Print excel data in console
        	
            System.out.print(row.getCell(j).getStringCellValue()+"|| ");
        }
        System.out.println();
    }
  }
    //Main function is calling readExcel function to read data from excel file
    public static void main(String[] args) throws IOException{
    //Create a object of ExcelFile class
    ReadFromExcel objExcelFile = new ReadFromExcel();
    //Prepare the path of excel file
    String filePath = System.getProperty("user.dir")+"\\src\\Excel";   //"C:\\workspace\\POI\\src\\excelExportAndFileIO";
    //System.getProperty("user.dir")+"\\src\\Excel","Employee.xlsx
    
    //Call read file method of the class to read data
    objExcelFile.readExcel(filePath,"TestAndQuiz.xlsx","Sheet1");
    //objExcelFile.readExcel(filePath,"ExportExcel.xlsx","Sheet3");
    }
}*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class LibReadWriteExcel {

	static XSSFWorkbook wb; // this is POI library referecne to a work book
	static XSSFSheet sheet; // this is POI library referencce to a sheet

	public static String getURL(String filePath, int Row, int Column, int Sheet) throws IOException{
		File src = new File(filePath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet=wb.getSheetAt(Sheet);
		String data = sheet.getRow(Row).getCell(Column).getStringCellValue();
		//System.out.println("Value= " + data);
		return data;
	}
	
	public static void writeresult(String filePath, int Row, int Column, int Sheet, String Value) throws IOException{
		File src = new File(filePath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		
		sheet=wb.getSheetAt(Sheet);
		//if(Sheet!=0)
		sheet.createRow(Row);
		sheet.getRow(Row).createCell(Column).setCellValue(Value);
		//System.out.println("ROW###"+sheet.getRow(Row));
		
		FileOutputStream fout=new FileOutputStream(new File(filePath));
		wb.write(fout);
		//System.out.println("Test Case Passed");
	
	}
	
	public static String getData(String filepath, int sheetnumber, int rownumber, int colnum) throws IOException {
		//System.out.println("Inside getData method");
		File src = new File(filepath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet = wb.getSheetAt(sheetnumber);
		//System.out.println("Column###"+colnum);
		String data = sheet.getRow(rownumber).getCell(colnum)
				.getStringCellValue();
		return data;
	}
	
	
	public int getRowCount(int sheetIndex) {
		System.out.println("Inside getRowsCount method");
		int rowcount = wb.getSheetAt(sheetIndex).getLastRowNum();
		return rowcount;
	}
	
	public static int getRowsCount(String filepath, int sheetIndex)  throws IOException{
		File src = new File(filepath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		int rowcount = wb.getSheetAt(sheetIndex).getLastRowNum();
		return rowcount;
	}
	
	public static int getColumnCount(String filepath, int sheetIndex) throws IOException{
		File src = new File(filepath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheet = wb.getSheetAt(sheetIndex);
		int colCount =sheet.getRow(0).getLastCellNum();
		return colCount;
	}
	
	public static int getSheetCount(String filepath) throws IOException{
		int sheetCount = 0;
		File src = new File(filepath);
		FileInputStream fis = new FileInputStream(src);
		wb = new XSSFWorkbook(fis);
		sheetCount = wb.getNumberOfSheets();
	 return sheetCount;
	}
}	
