package Package1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
public class ReadFromExcel {
	public void readExcel(String filePath,String fileName,String sheetName) throws IOException{
   //Create a object of File class to open xlsx file
   File file =    new File(filePath+"\\"+fileName);
   //Create an object of FileInputStream class to read excel file
    FileInputStream inputStream = new FileInputStream(file);
    
    Workbook excelWorkbook = null;
    //Find the file extension by splitting file name in substring and getting only extension name
    String fileExtensionName = fileName.substring(fileName.indexOf("."));
    excelWorkbook = new XSSFWorkbook(inputStream); 
    
  }
    
    public static void main(String[] args) throws IOException{
    //Create a object of ExcelFile class
    ReadFromExcel objExcelFile = new ReadFromExcel();
    //Prepare the path of excel file
   // String filePath = System.getProperty("user.dir")+"\\src\\Excel";   
        
    String myFile = System.getProperty("user.dir")+"\\src\\Excel\\TestAndQuiz.xlsx";
    
    int rowCount = LibReadWriteExcel.getRowsCount(myFile, 0);
    int colCount = LibReadWriteExcel.getColumnCount(myFile, 0);
    int sheetCount = LibReadWriteExcel.getSheetCount(myFile);
    
    String rowData = "";
    System.out.println("Row Count ----"+ rowCount);
 
    for(int k=0;k<sheetCount;k++) {
    	System.out.println("Sheet:" + k);
    	rowCount = LibReadWriteExcel.getRowsCount(myFile, k);
        colCount = LibReadWriteExcel.getColumnCount(myFile, k);
        
		    for (int i=0;i<rowCount;i++) {
		    	rowData = "";
		    	for(int j=0;j<colCount;j++)
		    	{
		    		if(j==colCount-1) {
		    		rowData += LibReadWriteExcel.getURL(myFile, i, j, k) ;
		    		}
		    		else {
		    			rowData += LibReadWriteExcel.getURL(myFile, i, j, k) + "| ";
		        			
		    		}
		    	}
		    	//System.out.println(LibReadWriteExcel.getURL(myFile, i, 0, "sheet1") +"," +LibReadWriteExcel.getURL(myFile, i, 1, "sheet1")+"," + LibReadWriteExcel.getURL(myFile, i, 2, "sheet1"));
		    	System.out.println(rowData);
		    }
		    //ReadExcelTestAndQuiz.writeresult(myFile, 0, 0, 1, LibReadWriteExcel.getURL(myFile, 0, 0, "sheet1"));
    }
   
    }
}
