package Package1;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.testng.annotations.Test;

public class PomFileNew {
	private static WebElement element = null;
	private static WebElement target = null;
	public static WebElement test_link(WebDriver driver) {
		element = driver.findElement(By.xpath("//a[text( )='This is a link']"));
		return element;
	}
	

	public static WebElement test_textbox(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='fname']"));
		return element;
	}

	public static WebElement test_button(WebDriver driver) throws InterruptedException {
		Thread.sleep(1000);
		element = driver.findElement(By.xpath("//*[text( )='Submit']"));
		return element;
	}

	public static WebElement test_radiobutton(WebDriver driver) {
		element = driver.findElement(By.xpath("//input[@id='female']"));
		return element;
	}

	public static WebElement test_checkbox1(WebDriver driver) {
		element = driver.findElement(By.xpath("//form/input[1][@class='Automation']"));
		return element;
	}

	public static WebElement test_checkbox2(WebDriver driver) {
		JavascriptExecutor js = (JavascriptExecutor) driver;
 		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
 		//Thread.sleep(2000);
		element = driver.findElement(By.xpath("//form/input[2][@class='Performance']"));
		return element;
	}

	public static WebElement test_dropdown(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='testingDropdown']"));
		Select select = new Select(driver.findElement(By.xpath("//*[@id='testingDropdown']")));
		select.selectByValue("Performance");

		return element;
	}

	public static WebElement test_doubleclick(WebDriver driver) throws InterruptedException {
	Thread.sleep(2000);
		element = driver.findElement(By.id("dblClkBtn"));
		Actions action = new Actions(driver);
		element = driver.findElement(By.id("dblClkBtn"));
		action.doubleClick(element).build().perform();
		Alert alert = driver.switchTo().alert();
		String alertMessage = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		System.out.println(alertMessage);
		return element;
	}

	public static WebElement test_alertbox(WebDriver driver) throws InterruptedException {
		Thread.sleep(2000);
		element = driver.findElement(By.xpath("//button[text( )='Generate Alert Box']"));
		element = driver.findElement(By.xpath("//button[text( )='Generate Alert Box']"));
		Actions action = new Actions(driver);
		action.click(element).build().perform();
		Alert alert = driver.switchTo().alert();
		String alertMessage = driver.switchTo().alert().getText();
		driver.switchTo().alert().accept();
		System.out.println("print alertbox1"+" "+alertMessage);
		return element;
		
	}	
	public static WebElement test_confirmbox(WebDriver driver) throws InterruptedException {	
	       element = driver.findElement(By.xpath("//button[text( )='Generate Confirm Box']"));
			element = driver.findElement(By.xpath("//button[text( )='Generate Confirm Box']"));
			Actions action = new Actions(driver);
			action.click(element).build().perform();
		       driver.switchTo().alert();
			String alertMessage = driver.switchTo().alert().getText();
			driver.switchTo().alert().dismiss();
			System.out.println(alertMessage);
			return element;
			
		       
	//public static WebElement test_confirmbox(WebDriver driver) throws InterruptedException {
		/*Thread.sleep(2000);
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(10, TimeUnit.SECONDS).pollingEvery(3, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);            
                  
               Alert alert = wait.until(new Function<WebDriver, Alert>() {       

   public Alert apply(WebDriver driver) {

 try {

    return driver.switchTo().alert();

} catch(NoAlertPresentException e) {

    return null;
     }
   }  
});
        alert.accept();*/
	
      /*  element = driver.findElement(By.xpath("//button[text( )='Generate Confirm Box']"));
		element = driver.findElement(By.xpath("//button[text( )='Generate Confirm Box']"));
		Actions action = new Actions(driver);
		action.doubleClick(element).build().perform();
	       driver.switchTo().alert();
		String alertMessage = driver.switchTo().alert().getText();
		driver.switchTo().alert().dismiss();
		System.out.println(alertMessage);
		return element;*/
		
			
}
	public static WebElement test_dragdrop(WebDriver driver) throws InterruptedException {
		Thread.sleep(10000);
		element = driver.findElement(By.id("sourceImage"));
		 target = driver.findElement(By.id("targetDiv"));
		Actions action = new Actions(driver);
		Thread.sleep(10000);
		action.clickAndHold(element).moveToElement(target).release().build().perform();
		
		return element;
	}

	
	  @Test
	  public void f() {
		  
		  
	  }
  
  
}
