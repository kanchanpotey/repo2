package Package1;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebElement;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestandQuiz {
	public static WebDriver driver = null;
	
	@BeforeTest                         
	 public void browser() {
   System.setProperty("webdriver.gecko.driver","C:\\Selenium\\geckodriver\\geckodriver.exe");
                      
   String baseUrl = "https://www.testandquiz.com/selenium/testing.html";
   driver = new FirefoxDriver();
   driver.manage().window().maximize();
   driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
   driver.get(baseUrl);
	}
	
	 //@Test(priority=0)
	public void readExcel(String filePath,String fileName,String sheetName) throws IOException{
		    File file =    new File(filePath+"\\"+fileName);
		    FileInputStream inputStream = new FileInputStream(file);
		    
		    Workbook excelWorkbook = null;
		    String fileExtensionName = fileName.substring(fileName.indexOf("."));
		    excelWorkbook = new XSSFWorkbook(inputStream); 
		    
		  }
		    
		    public static void main(String[] args) throws IOException{
		     ReadFromExcel objExcelFile = new ReadFromExcel();
		     String myFile = System.getProperty("user.dir")+"\\src\\Excel\\TestAndQuiz.xlsx";
		    
		    int rowCount = LibReadWriteExcel.getRowsCount(myFile, 0);
		    int colCount = LibReadWriteExcel.getColumnCount(myFile, 0);
		    int sheetCount = LibReadWriteExcel.getSheetCount(myFile);
		    
		    String rowData = "";
		    System.out.println("Row Count ----"+ rowCount);
		 
		    for(int k=0;k<sheetCount;k++) {
		    	System.out.println("Sheet:" + k);
		    	rowCount = LibReadWriteExcel.getRowsCount(myFile, k);
		        colCount = LibReadWriteExcel.getColumnCount(myFile, k);
		        
				    for (int i=0;i<rowCount;i++) {
				    	rowData = "";
				    	for(int j=0;j<colCount;j++)
				    	{
				    		if(j==colCount-1) {
				    		rowData += LibReadWriteExcel.getURL(myFile, i, j, k) ;
				    		}
				    		else {
				    			rowData += LibReadWriteExcel.getURL(myFile, i, j, k) + "| ";
				        			
				    		}
				    	}
				    	//System.out.println(LibReadWriteExcel.getURL(myFile, i, 0, "sheet1") +"," +LibReadWriteExcel.getURL(myFile, i, 1, "sheet1")+"," + LibReadWriteExcel.getURL(myFile, i, 2, "sheet1"));
				    	System.out.println(rowData);
				    }
				    //ReadExcelTestAndQuiz.writeresult(myFile, 0, 0, 1, LibReadWriteExcel.getURL(myFile, 0, 0, "sheet1"));
		    }
		   
   }
		

		
	    @Test(priority=1)
	        public void link(){
	     	PomFileNew.test_link(driver).click();
		    driver.navigate().back();
	}
	  
    	@Test(priority=2)
        	public void textbox() throws InterruptedException{
		      Thread.sleep(3000);
		      PomFileNew.test_textbox(driver).sendKeys("Selenium");
        	}	  
		
		@Test(priority=3)
	         public void button() throws InterruptedException{
		    	 Thread.sleep(3000);
		       PomFileNew.test_button(driver).click();
	        }
		
		@Test(priority=4)
	         public void radioButton(){
		     PomFileNew.test_radiobutton(driver).click();
        	}
		
		@Test(priority=5)
	       public void checkBox1(){
			PomFileNew.test_checkbox1(driver).click();
	       }
		
		@Test(priority=6)
	       public void checkBox2(){
			PomFileNew.test_checkbox2(driver).click();
			}
		
		@Test(priority=7)
	    	public void dropdown(){
			PomFileNew.test_dropdown(driver).click();
			}
		
		@Test(priority=8)
		    public void doubleClick() throws InterruptedException{
			Thread.sleep(4000);
			PomFileNew.test_doubleclick(driver).click();		
			}
		
         @Test (priority=9)
		    public void alterBox() throws InterruptedException{
			Thread.sleep(3000);
			PomFileNew.test_alertbox(driver).click();
		    }
         
       @Test (priority=10)
            public void confirmBox1() throws InterruptedException{
	        Thread.sleep(3000);
	        PomFileNew.test_confirmbox(driver).click();
	      
	      
           }
       
       @Test ()
            public void dragdrop() throws InterruptedException{
	        Thread.sleep(3000);
	        PomFileNew.test_dragdrop(driver).click();
	      
           }
		
       @AfterTest
   	       public void closeBrowser() {
   		   //driver.quit();
         }
}





















